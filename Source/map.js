var hospital = {};
var markers=[];
var that =  this;
var visible_markers=[];
var compare_nodes=[];
var marker12;
hospital.properties = function () {
  this.execute = function () {
    that.render();
  };
  that.render = function() {
    
    $("#list-area").css("overflow","auto")
                .css("height",$(window).height());
   
    $("#review").css("top",-1000)
    
    $("#search_name").keyup(function(){
      var text = $("#search_name")
      var search_text =text.val();
      var search_result=[];
      if(search_text.length>3){
        console.log(search_text);
          $("#hospital-list").empty();
        for(var i =0;i<that.data.length;i++){
         if(that.data[i].hospital_name.toLowerCase().indexOf(search_text) > -1){
            div = document.createElement('div');
            div.id=i+"div-list";
            div.className="single-hospital-details";
            div.innerHTML="<div class='left-hospital-details'>"+
                    "<p class='list-hospital-name'>"+that.data[i].hospital_name+"</p>"+
                    // "<span class='pull-right preferred-tag'><i>Preferred</i></span>"+
                    "<br>"+
                    "<p style='margin-bottom: 5px;'>"+that.data[i].address+"</p>"+
                     "<button style='background-color:#3a5795;color:white;margin-bottom: 5px;'>Share on Fb</button>"+
                    "<div style='width:350px'>"+
                     "<button type='button' class='btn btn-default compare-and-book-buttons'>Add to Compare</button>"+
                      "<button type='button' class='btn btn-default compare-and-book-buttons review-button' id='reviews1'>Reviews & more</button>"+
                      "<div class='pull-right'>"+
                        "<img src='assets/red-flag.png' height='34' width='34'> <b>"+that.data[i].red_flags+"</b>"+
                      "</div>"+
                    "</div>"+
                  "</div>"  ;
        $("#hospital-list").append(div);
            console.log(that.data[i].hospital_name,"<<<<<<<<<<,")
          }
        }
      }else if(search_text.length==0){
        createDiv(visible_markers);
      }

    })

    $("#add-review").on("click",function(){
      $("#content").css("display","block")
      $("#review-form").css("transition","height 0.9s")
        .css("height","auto")
      $("#add-review").css("visibility","hidden")
      $("#close-review").css("visibility","visible")
    })
    $("#close-review").on("click",function(){
      $("#content").css("display","none")
      $(".more-review-question").css("display","none")
      $("#review-form").css("transition","height 0.9s")
        .css("height","0px")
      $("#close-review").css("visibility","hidden")
      $("#add-review").css("visibility","visible")
    })
    $("#list-area").css("left","-1000px").css("top",0).css("position","absolute")
    $("#body-filter").css("left","-500px").css("top",0).css("position","absolute")
    $('.slider-input').jRange({
      from: 0,
      to: 5,
      step: 1,
      scale: [0,1,2,3,4,5],
      format: '%s',
      width: 200,
      showLabels: true,
      showScale: false,
      theme: "theme-blue"
    });
    $("#pull_to_filter").css("visibility","hidden")
    $("#compareid").css("top",-1000)
      .css("height","100%")
      .css("width","100%")
      .css("position","absolute");
    $(".compare-button").on("click",function() {  
      div = document.createElement('div');
            div.id="compare-page";
            div.className="compare-list-page";
            try{
              div.innerHTML="<h3 class='compare-page-heading'>Comparison Chart</h3><table class='table'> <thead> <tr> <th></th> <th>"+compare_nodes[0].data.hospital_name+"</th> <th>"+compare_nodes[1].data.hospital_name+"</th> <th>"+compare_nodes[2].data.hospital_name +"</th> <th>City Average</th> </tr></thead> <tbody> <tr> <td>Overall Rating</td><td> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/empty-star.png'> </td><td> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> </td><td> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/empty-star.png'> <img src='assets/empty-star.png'> </td><td> <img src='assets/filled-stars.png'> <img src='assets/filled-stars.png'> <img src='assets/empty-star.png'> <img src='assets/empty-star.png'> <img src='assets/empty-star.png'> </td></tr><tr> <td>Distance</td><td>2.5 km</td><td>7.9 km</td><td>8.2 km</td><td>-</td></tr><tr> <td>Grade</td><td>"+compare_nodes[1].data.grade+"</td><td>"+compare_nodes[1].data.grade+"</td><td>"+compare_nodes[2].data.grade+"</td><td>Grade A <div class='caret'></div></td></tr><tr> <td>Speciality</td><td>"+compare_nodes[1].data.type_of_hospital+"</td><td>"+compare_nodes[1].data.type_of_hospital+"</td><td>"+compare_nodes[1].data.type_of_hospital+"</td><td>All<div class='caret'></div></td></tr><tr> <td>Room Rate</td><td>"+compare_nodes[0].data.bed_charges+"</td><td>"+compare_nodes[1].data.bed_charges+"</td><td>"+compare_nodes[2].data.bed_charges+"</td><td>Average 3,500</td></tr><tr> <td>Condition of facilities</td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td></tr><tr> <td>Quality of service</td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td></tr><tr> <td>Care provided</td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td></tr><tr> <td>Cost</td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td><td> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/filled-circle.png'> <img src='assets/empty-circle.png'> <img src='assets/empty-circle.png'> </td></tr><tr> <td colspan='5' class='enter-procedure-td'> <div class='form-group has-feedback'> <label class='control-label'>Enter Procedure :</label> <input type='text' class='form-control search-box-style' placeholder='Search condition'> <span class='glyphicon glyphicon-search form-control-feedback search-icon' aria-hidden='true'></span> </div></td></tr><tr> <td>Procedure cost</td><td>5,70,000</td><td>6,40,000</td><td>3.50.000</td><td>-</td></tr><tr> <td></td><td><button type='button' class='btn btn-default compare-and-book-buttons'>Remove</button></td><td><button type='button' class='btn btn-default compare-and-book-buttons'>Remove</button></td><td><button type='button' class='btn btn-default compare-and-book-buttons'>Remove</button></td><td>-</td></tr></tbody></table>";
            }
            catch(err){
              console.log("no value selected")
            }
            $("#compareid").append(div)
      $("#compareid").css("transition","top 0.9s")
        .css("top",0)
      $("#body-filter").css("visibility","hidden")
      $("#list-area").css("visibility","hidden")
      $("#review").css("visibility","hidden")
      compare_nodes=[];
    });

    $(".back-compare").on("click",function() {
      console.log("fired back")
      $("#compareid").css("top",-1000+"px");
      $("#body-filter").css("visibility","visible")
      $("#list-area").css("visibility","visible")
    });

    $(".more-review").css("color","blue")
      .css("text-decoration","underline");
    $(".more-review").on("click",function () {
      $(".slider-review-portion").css("height","440px");
      $(".more-review-question").css("display","block");

    })
    $("#hospital-list").on("click",'.list-hospital-name',function(){ 
      // $($(this)[0].parentNode.parentNode).css("display","block")
      //   .css("height",$(window).height());
      var div_id = $("#"+$(this)[0].parentNode.parentNode.id);
       if (!div_id.hasClass("no-action1")){
        $("#back_list").css("display","block")
        div_id.attr("class","no-action1")
        var previous_html = div_id.html();
        var id_div = div_id[0].id;
        console.log(div_id[0].id)
        div_id.html(previous_html + "<div class='hospital-info'> <div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'> <div class='panel hospital-info-panel panel-default'> <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne' aria-expanded='true' aria-controls='collapseOne'> <div class='panel-heading hospital-info-panel-heading' role='tab' id='headingOne'> <h5 class='panel-title hospital-info-panel-title'> General Info </h5> </div></a> <div id='collapseOne' class='panel-collapse collapse in hospital-info-panel-collapse' role='tabpanel' aria-labelledby='headingOne'> <div class='panel-body hospital-info-panel-body'> <table class='table hospital-info-table'> <tbody> <tr> <td>Type</td><td>"+that.data[parseInt(id_div)].type_of_hospital+"</td></tr><tr> <td>Beds</td><td>"+that.data[parseInt(id_div)].Beds+"</td></tr><tr> <td>OPD</td><td>"+that.data[parseInt(id_div)].opd+"</td></tr><tr> <td>IPD</td><td>"+that.data[parseInt(id_div)].ipd+"</td></tr><tr> <td>Visiting Hours</td><td>"+that.data[parseInt(id_div)].visiting_hours+"</td></tr><tr> <td>Website</td><td>"+that.data[parseInt(id_div)].website+"</td></tr><tr> <td>Toll Free</td><td>"+that.data[parseInt(id_div)].tollfree_no+"</td></tr><tr> <td>24 Hours</td><td>"+that.data[parseInt(id_div)].alltime_helpline+"</td></tr><tr> <td>Emergency</td><td>"+that.data[parseInt(id_div)].emergency_no+"</td></tr></tbody> </table> </div></div></div><div class='panel hospital-info-panel panel-default'> <a class='collapsed' data-toggle='collapse' data-parent='#accordion' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'> <div class='panel-heading hospital-info-panel-heading' role='tab' id='headingTwo'> <h5 class='panel-title hospital-info-panel-title'> Procedures & Cost </h5> </div></a> <div id='collapseTwo' class='panel-collapse collapse hospital-info-panel-collapse' role='tabpanel' aria-labelledby='headingTwo'> <div class='panel-body hospital-info-panel-body'> <table class='table hospital-info-table'> <tbody> <tr> <td>caesarean section</td><td>"+that.data[parseInt(id_div)].caesarean_section+"</td></tr><tr> <td>cataract</td><td>"+that.data[parseInt(id_div)].cataract+"</td></tr><tr> <td>cholecystectomy</td><td>"+that.data[parseInt(id_div)].cholecystectomy+"</td></tr><tr> <td>Normal Delivery</td><td>"+that.data[parseInt(id_div)].normal_delivery+"</td></tr><tr> <td>PTCA</td><td>"+that.data[parseInt(id_div)].ptca+"</td></tr><tr> <td>Appendectomy</td><td>"+that.data[parseInt(id_div)].appendectomy+"</td></tr><tr> <td>MTP</td><td>"+that.data[parseInt(id_div)].mtp+"</td></tr></tbody> </table> </div></div></div></div></div>");
       $(".single-hospital-details").not(document.getElementById( div_id ))
        .css("display","none");
        console.log($(".single-hospital-details").not(document.getElementById( div_id )))
       }
      $("#back_list").on("click",function(){
        event.preventDefault();
        $(".hospital-info").empty();
        div_id.removeClass("no-action1");
        div_id.attr("class","single-hospital-details");
        $(".single-hospital-details")
        .css("display","block");
        console.log($(".single-hospital-details").not(document.getElementById( div_id ))) 
        $("#back_list").css("display","none")    
      });
    });
  }
}

var map = undefined;

  function initialize() {
    var mapOptions = {
      center: { lat: 19.1653955, lng: 72.9313767},
      zoom: 12,
      disableDefaultUI: true,
      minZoom:7
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
    var styles = [
      {
        "stylers":[
          {
            "hue":"#ff1a00"
          },
          {
            "invert_lightness":true
          },
          {
            "saturation":-100
          },
          {
            "lightness":33
          },
          {
            "gamma":0.5
          }
        ]
      },
      {
        "featureType":"water",
        "elementType":"geometry",
        "stylers":[
          {
            "color":"#2D333C"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
              "lightness": 20
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
              "color": "#000000"
            },
            {
                "lightness": 19
            },
            {
              "visibility": "off"
            }
        ]
    },
    ]

    map.setOptions({styles: styles});
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  function clickButton() {
      navigator.geolocation.getCurrentPosition(success);
      function success(position) {
           var latitude = position.coords.latitude;
           var longitude = position.coords.longitude;
        marker12 = new google.maps.Marker({
          position: new google.maps.LatLng(latitude, longitude),
          icon:'assets/marker.png',
          map: map,
          id: "location"
        });
         var infowindow = new google.maps.InfoWindow();
    infowindow.setContent('You');
    infowindow.open(map, marker12);
    setTimeout(function () { infowindow.close(); }, 2500);
        map.setCenter(new google.maps.LatLng(latitude, longitude));
        // Add the circle for this city to the map.
      }

   
    $(".gmnoprint").css("visibility","hidden");
    $("#page1").css("transition","top 0.9s")
      .css("top","-1000px");

    $("#location_near_you").css("visibility","visible")
    d3.csv("hospital.csv", function (data) {
      that.data=data;
      console.log(data);
      // data.each(function (data) {})
      var marker, i;

      for (i = 0; i < data.length; i++) { 
        mapid = i+"marker"; 
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(data[i]["lat"], data[i]["long"]),
          icon:'assets/map-pointer.png',
          map: map,
          id: mapid,
          name:data[i].short_name
        });
        markers.push(marker);
        $("#pull_to_filter").css("visibility","visible")
        google.maps.event.addListener(marker, 'click', function(e) {
          $("#hospital-list").empty();
            width = $("#list-area").width();
            div = document.createElement('div');
            div.id=this.id+"div-list";
            div.className="single-hospital-details";
            console.log(parseInt(this.id),"<<<<");
            div.innerHTML="<div class='left-hospital-details'>"+
                    "<p class='list-hospital-name'>"+that.data[parseInt(this.id)].hospital_name+"</p>"+
                    // "<span class='pull-right preferred-tag'><i>Preferred</i></span>"+
                    "<br>"+
                    "<p style='margin-bottom: 5px;'>"+that.data[parseInt(this.id)].address+"</p>"+
                     "<button style='background-color:#3a5795;color:white;margin-bottom: 5px;'>Share on Fb</button>"+
                    "<div style='width:350px'>"+
                     "<button type='button' class='btn btn-default compare-and-book-buttons'>Add to Compare</button>"+
                      "<button type='button' class='btn btn-default compare-and-book-buttons review-button' id='reviews1'>Reviews & more</button>"+
                      "<div class='pull-right'>"+
                        "<img src='assets/facebook-icon.png' height='34' width='34'>"+
                        "<img src='assets/red-flag.png' height='34' width='34'> <b>"+that.data[parseInt(this.id)].red_flags+"</b>"+
                      "</div>"+
                    "</div>"+
                  "</div>"+"<div class='hospital-info'> <div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'> <div class='panel hospital-info-panel panel-default'> <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne' aria-expanded='true' aria-controls='collapseOne'> <div class='panel-heading hospital-info-panel-heading' role='tab' id='headingOne'> <h5 class='panel-title hospital-info-panel-title'> General Info </h5> </div></a> <div id='collapseOne' class='panel-collapse collapse in hospital-info-panel-collapse' role='tabpanel' aria-labelledby='headingOne'> <div class='panel-body hospital-info-panel-body'> <table class='table hospital-info-table'> <tbody> <tr> <td>Type</td><td>"+that.data[parseInt(this.id)].type_of_hospital+"</td></tr><tr> <td>Beds</td><td>"+that.data[parseInt(this.id)].Beds+"</td></tr><tr> <td>OPD</td><td>"+that.data[parseInt(this.id)].opd+"</td></tr><tr> <td>IPD</td><td>"+that.data[parseInt(this.id)].ipd+"</td></tr><tr> <td>Visiting Hours</td><td>"+that.data[parseInt(this.id)].visiting_hours+"</td></tr><tr> <td>Website</td><td>"+that.data[parseInt(this.id)].website+"</td></tr><tr> <td>Toll Free</td><td>"+that.data[parseInt(this.id)].tollfree_no+"</td></tr><tr> <td>24 Hours</td><td>"+that.data[parseInt(this.id)].alltime_helpline+"</td></tr><tr> <td>Emergency</td><td>"+that.data[parseInt(this.id)].emergency_no+"</td></tr></tbody> </table> </div></div></div><div class='panel hospital-info-panel panel-default'> <a class='collapsed' data-toggle='collapse' data-parent='#accordion' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'> <div class='panel-heading hospital-info-panel-heading' role='tab' id='headingTwo'> <h5 class='panel-title hospital-info-panel-title'> Procedures & Cost </h5> </div></a> <div id='collapseTwo' class='panel-collapse collapse hospital-info-panel-collapse' role='tabpanel' aria-labelledby='headingTwo'> <div class='panel-body hospital-info-panel-body'> <table class='table hospital-info-table'> <tbody> <tr> <td>caesarean section</td><td>"+that.data[parseInt(this.id)].caesarean_section+"</td></tr><tr> <td>cataract</td><td>"+that.data[parseInt(this.id)].cataract+"</td></tr><tr> <td>cholecystectomy</td><td>"+that.data[parseInt(this.id)].cholecystectomy+"</td></tr><tr> <td>Normal Delivery</td><td>"+that.data[parseInt(this.id)].normal_delivery+"</td></tr><tr> <td>PTCA</td><td>"+that.data[parseInt(this.id)].ptca+"</td></tr><tr> <td>Appendectomy</td><td>"+that.data[parseInt(this.id)].appendectomy+"</td></tr><tr> <td>MTP</td><td>"+that.data[parseInt(this.id)].mtp+"</td></tr></tbody> </table> </div></div></div></div></div>";

        $("#hospital-list").append(div);
            console.log($("#pull_to_filter").css("visibility") === "hidden");
            if(!($("#pull_to_filter").css("visibility") === "hidden")) {
              $("#list-area").css("transition","left 0.9s")
                .css("left",($(window).width() - width))
                .css("height",$(window).height())
                .css("position","absolute")
                .css("background","rgba(255,255,255,0.7)")
                .css("top",0);
            }
          
        });

      }
      
    })  

}


function pullToFilter () {
  var radiuskm = $("#radiuskm");
  radiuskm.css("display","inline"); 
  var donut;
  var pull = document.getElementById('pull_to_filter');
  var bounds = new google.maps.LatLngBounds();
  var factor=1;
  $("#back-to-map").on("click",function(event){
    event.preventDefault();
   $("#hospital-list").empty();
   for (var i = 0; i < markers.length; i++) { 
    markers[i].setMap(null);
   }
   visible_markers=[];
    $("#list-area").css("transition","left 0.9s")
      .css("left",-1000+"px")
      .css("background","transparent")
    $("#body-filter").css("transition","left 0.9s")
      .css("left","-1000px")
      setTimeout(function(){donut.setMap(null);$("#pull_to_filter").css("visibility","visible")},400);
      google.maps.event.clearListeners(map, 'zoom_changed');
      google.maps.event.clearListeners(map, 'dragend');
      google.maps.event.clearListeners(map, 'center_changed');
      markers=[];
      for (var i = 0; i < that.data.length; i++) { 
        mapid = i+"marker"; 
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(that.data[i].lat,that.data[i]["long"]),
          icon:'assets/map-pointer.png',
          map: map,
          id: mapid,
          name:that.data[i]["short_name"]
        });
      markers.push(marker);
      }
      radiuskm.css("display","none");
  });
$(".compare-button").prop("disabled","true");
$(".compare-button").css("opacity","0.5")
  function drawCircle(point, radius, dir) { 
    var d2r = Math.PI / 180;   // degrees to radians 
    var r2d = 180 / Math.PI;   // radians to degrees 
    var earthsradius = 3963; // 3963 is the radius of the earth in miles

       var points = 50; 

       // find the raidus in lat/lon 
       var rlat = (radius / earthsradius) * r2d; 
       var rlng = rlat / Math.cos(point.lat() * d2r); 


       var extp = new Array(); 
       if (dir==1)  {var start=0;var end=points+1} // one extra here makes sure we connect the
       else     {var start=points+1;var end=0}
       for (var i=start; (dir==1 ? i < end : i > end); i=i+dir)  
       { 
          var theta = Math.PI * (i / (points/2)); 
          ey = point.lng() + (rlng * Math.cos(theta)); // center a + radius x * cos(theta) 
          ex = point.lat() + (rlat * Math.sin(theta)); // center b + radius y * sin(theta) 
          extp.push(new google.maps.LatLng(ex, ey)); 
          bounds.extend(extp[extp.length-1]);
       } 
       // alert(extp.length);
       return extp;
     }
      var zoom_level = map.getZoom();
      factor = Math.pow(2,(14-zoom_level));
       donut = new google.maps.Polygon({
                 paths: [drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), factor, 1),
                         drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), 10000, -1)],
                 strokeColor: "transparent",
                 strokeOpacity: 0.8,
                 strokeWeight: 2,
                 fillColor: "white",
                 fillOpacity: 0.8
      
     });
     donut.setMap(map);
     setTimeout(function(){
      for(var i = 0;i<markers.length;i++){
        if (google.maps.geometry.poly.containsLocation(markers[i].position, donut)){
         markers[i].setMap(null);
       // console.log(markers[i],"visible");
        }
        if (!google.maps.geometry.poly.containsLocation(markers[i].position, donut)){
          visible_markers.push(markers[i])
          markers[i].setMap(map);
        }
        
    }createDiv(visible_markers);},400);

  $("#hospital-list").on("click",'.compare-and-book-buttons',function(){
    var node = $($($(this)[0].parentNode.parentNode.parentNode));
     var node_id=$(node)[0].id;
     if($(this).hasClass("compare-fill")){
      console.log("true")
      $(this).removeClass("compare-fill")
      compare_nodes = jQuery.grep(compare_nodes, function(value) {
        console.log(value,"value<<");
        if(value.index != parseInt(node_id)){
          return value;
        }
      });
    }else
    {
      console.log("false")
      $(this).addClass("compare-fill")
      compare_nodes.push({data:that.data[parseInt(node_id)],index:parseInt(node_id)});
    }
     //$(this).addClass("compare-fill");
     if(compare_nodes.length>=3){
      $(".compare-button").css("opacity",1)
      $(".compare-button").prop("disabled",false);
     }else{
      $(".compare-button").prop("disabled",true);
      $(".compare-button").css("opacity","0.5")
     }
    console.log(compare_nodes);
  })


   $("#hospital-list").on("click",'.review-button',function(){ 
   google.maps.event.clearListeners(map, 'center_changed');
      $("#review").css("width","66%")
        .css("visibility","visible")
      $("#review").css("transition","top 0.9s")
        .css("top",0)
        .css("height",$(window).height())
        .css("position","absolute")
        .css("top",0);
      $("#body-filter").css("visibility","hidden")
      $("#focus").css("visibility","hidden");
    donut.setMap(null);
     donut = new google.maps.Polygon({
         paths: [drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), factor, -1),
                  drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), 10000, -1)],
         strokeColor: "transparent",
         strokeOpacity: 0.8,
         strokeWeight: 2,
         fillColor: "white",
         fillOpacity: 0.8
      
     });
     donut.setMap(map);
    })   
  if (!google.maps.Polygon.prototype.getBounds) {
 
    google.maps.Polygon.prototype.getBounds=function(){
        console.log(this,"this");
        var bounds = new google.maps.LatLngBounds();
        this.getPath().forEach(function(element,index){bounds.extend(element)})
        return bounds;
    }
 
  }

  google.maps.event.addListener(map, 'zoom_changed', function() {
      console.log(factor,"increased");
      factor = Math.pow(2,(14-map.getZoom()));
      donut.setMap(null);
      $("#radius_value").attr("value",Math.round(factor*1.60934)+" km");
       donut = new google.maps.Polygon({
                 paths: [drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), factor, 1),
                         drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), 10000, -1)],
                 strokeColor: "transparent",
                 strokeOpacity: 0.8,
                 strokeWeight: 2,
                 fillColor: "white",
                 fillOpacity: 0.8
     });
     donut.setMap(map);

  var visible_markers=[];
    for(var i = 0;i<markers.length;i++){
      if (google.maps.geometry.poly.containsLocation(markers[i].position, donut)){
        markers[i].setMap(null);
       // console.log(markers[i],"visible");
      }
      if (!google.maps.geometry.poly.containsLocation(markers[i].position, donut)){
        visible_markers.push(markers[i])
        markers[i].setMap(map);
      }
    }
        createDiv(visible_markers);
  });
google.maps.event.addListener(map, "dragend", function() {
  var visible_markers=[];
  for(var i = 0;i<markers.length;i++){
      if (google.maps.geometry.poly.containsLocation(markers[i].position, donut)){
        markers[i].setMap(null);
        console.log(markers[i].visible,"visible");
      }
      if (!google.maps.geometry.poly.containsLocation(markers[i].position, donut)){
        visible_markers.push(markers[i])
        markers[i].setMap(map);
        console.log(markers[i].visible,"visible");
      }
    }
     createDiv(visible_markers);

});
 
  google.maps.event.addListener(map,"center_changed", function() {
    if (!this.mouseDown) {
        //user has clicked the pan button
         factor = Math.pow(2,(14-map.getZoom()));
      donut.setMap(null);
      console.log(factor,"increased");
       donut = new google.maps.Polygon({
                 paths: [drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), factor, 1),
                         drawCircle(new google.maps.LatLng(map.getCenter().k,map.getCenter().D), 10000, -1)],
                 strokeColor: "transparent",
                 strokeOpacity: 0.8,
                 strokeWeight: 2,
                 fillColor: "white",
                 fillOpacity: 0.8
     });
     donut.setMap(map);
   }            
  });

  $("#pull_to_filter").css("visibility","hidden")
  // $("#page2").css("transition","width 0.9s")
  //     .css("width","100%");
  var width =  $(window).width() - $("#list-area").width();
  $("#list-area").css("transition","left 0.9s")
    .css("left",width)
    .css("background","transparent")
  $("#body-filter").css("transition","left 0.9s")
    .css("left","0")
}
$("#review-submit").on("click",function(){
  console.log("inside submit");
  $("#popup1").css("display","inline")
})
function createDiv(markers){
  var div =[];
  console.log(markers,"<<<<")
  $("#hospital-list").empty();
  for (var i = 0;i < markers.length;i++) {
   div = document.createElement('div');
     div.id=parseInt(markers[i].id)+"div-list";
     div.className="single-hospital-details";
     div.innerHTML="<div class='left-hospital-details'>"+
              "<p class='list-hospital-name'>"+that.data[parseInt(markers[i].id)].hospital_name+"</p>"+
              // "<span class='pull-right preferred-tag'><i>Preferred</i></span>"+
              "<br>"+
              "<p style='margin-bottom: 5px;'>"+that.data[parseInt(markers[i].id)].address+"</p>"+
               "<button style='background-color:#3a5795;color:white;margin-bottom: 5px;'>Share on Fb</button>"+
              "<div style='width:350px'>"+
               "<button type='button' class='btn btn-default compare-and-book-buttons'>Add to Compare</button>"+
                "<button type='button' class='btn btn-default compare-and-book-buttons review-button' id='reviews1'>Reviews & more</button>"+
                "<div class='pull-right'>"+
                  "<img src='assets/facebook-icon.png' height='34' width='34'>"+
                  "<img src='assets/red-flag.png' height='34' width='34'> <b>"+that.data[parseInt(markers[i].id)].red_flags+"</b>"+
                "</div>"+
              "</div>"+
            "</div>";
    $("#hospital-list").append(div);
  }
}

