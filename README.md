# README #

Health Hackathon Project for Team 2

### What is this repository for? ###

* Hackathon sponsored by BFSI client to create a hospital discovery
* This meant to be an Idea Prototype, the end product (being early alpha) is very raw and tries to build on the User Interface design document found in the Design folder
* v0.1 (Early Alpha Release)

### How do I get set up? ###

* Clone the repository using SSH or HTTPS
* Add to your localhost folder or run with python
* from the command prompt $, run the following

```
#!cmd
$ git clone git@bitbucket.org:ZoneStartupsIn/team2.git
$ cd team2/Source
$ python -m SimpleHTTPServer

```
* Open the following in your browser: http://localhost:8000/map.html


### Who do I talk to? ###

* Contact us at akshay.rao@pykih.com
* Check out our past work at www.pykih.com